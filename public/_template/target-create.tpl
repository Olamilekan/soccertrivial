{include file="header.tpl"}
<body class="page-body page-fade gray">
<div class="page-container">
    {include file="side-bar.tpl"}

    <div class="main-content">
        {include file="top-bar.tpl"}

        <div class="row">
            <div class="col-md-12">

                <div class="well well-sm">
                    <h4><i class="entypo-layout"></i> Create Target</h4>
                </div>

                <div class="panel panel-primary" data-collapsed="0">

                    <div class="panel-body">
                            <p>
                                Drag or Browse more than one prepared target images <strong>(in {$TARGET_EXTENSION|replace:'|':' , '})</strong> not more than
                                        {$TARGET_SIZE_LIMIT/1000} KB.
                                <br>Successfully uploaded targets will be sync to cloud in a space of 3mins.
                                <br>Check <a href="{$BASE_URL}targets/view/" class="btn btn-default btn-xs">List of Targets</a> for Sync update
                            </p>
                        <br />

                        <form action="{$BASE_URL}targets/upload/" class="dropzone dz-min" id="dropzone_example">
                            <div class="fallback">
                                <input name="file" type="file" multiple />
                            </div>
                        </form>

                        <div id="dze_info" class="hidden">
                            <br />
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title">Dropzone Uploaded Images Info</div>
                                </div>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th width="40%">File name</th>
                                        <th width="15%">Size</th>
                                        <th width="15%">Type</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody> <tfoot>
                                    <tr>
                                        <td colspan="4">
                                        </td>
                                    </tr> </tfoot>
                                </table>
                            </div>
                        </div>
                        <br />
                    </div>
                </div>
            </div>
        </div>
        {include file="bottom-bar.tpl"}
    </div>
{include file="footer.tpl"}