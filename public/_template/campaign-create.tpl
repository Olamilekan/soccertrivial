{include file="header.tpl"}
<body class="page-body page-fade gray">
<div class="page-container">
    {include file="side-bar.tpl"}

    <div class="main-content">
        {include file="top-bar.tpl"}

        <div class="row">
            <div class="col-md-12">

                <div class="well well-sm">
                    <h4><i class="entypo-layout"></i> Create new Campaign</h4>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-body">

                        {if $error|default:''}
                            <div class="alert alert-danger">
                                <h3>Opps! Error Creating Campaign</h3>
                                {foreach from=$error item=err}
                                    <p>{$err}</p>
                                {/foreach}
                            </div>
                        {/if}

                        {if $okay|default:''}
                            <div class="alert alert-success">
                                <h3>Hey! Success Creating Campaign</h3>
                                <p>{$okay}</p>
                            </div>
                        {/if}

                        <form role="form" id="form1" action="{$BASE_URL}campaign/create/" method="post" class="validate">
                            <div class="form-group">
                                <label class="control-label">Campaign Title</label>
                                <input type="text" class="form-control" name="title" data-validate="required" data-message-required="This is field is required" placeholder="Enter Campaign Title" />
                            </div>


                            <div class="form-group">
                                <label class="control-label">Select Account</label>
                                <select class="form-control" name="account">
                                    <option value="">&larr; Choose an Account &rarr;</option>
                                    {foreach from=$account_list item=account}
                                        <option value="{$account.account_id}" name="account">{$account.name}</option>
                                    {/foreach}
                                </select>
                            </div>


                            <div class="form-group">
                                <label class="control-label">Duration</label>
                                <input type="number" class="form-control" name="duration" data-validate="required" data-message-required="This is field is required" placeholder="Enter Duration of Campaign" min="1"/>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Description</label>
                                <textarea class="form-control" name="description" data-validate="required" data-message-required="This is field is required"></textarea>
                            </div>

                            <div class="form-group text-right">
                                <button type="reset" class="btn">Reset</button>
                                <button type="submit" class="btn btn-success"><i class="entypo-check"></i> Create Campaign</button>
                            </div>
                        </form>
                    </div>
                </div>


            </div>
        </div>
        {include file="bottom-bar.tpl"}
    </div>
{include file="footer.tpl"}