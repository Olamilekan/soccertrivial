<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="{$APP_DESCRIPTION}" />
    <meta name="author" content="Laborator.co" />
    <title>{$APP_NAME}</title>
    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css" id="style-resource-1">
    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/css/font-icons/entypo/css/entypo.css" id="style-resource-2">
    <link href='http://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/css/bootstrap.css" id="style-resource-4">
    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/css/neon-core.css" id="style-resource-5">
    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/css/neon-theme.css" id="style-resource-6">
    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/css/neon-forms.css" id="style-resource-7">
    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/css/custom.css" id="style-resource-8">
    <script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/jquery-1.11.0.min.js"></script>
    <script>$.noConflict();</script>
    <!--[if lt IE 9]>
    <script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/ie8-responsive-file-warning.js">
    </script><![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js">
    </script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js">
    </script> <![endif]-->
    <!-- TS1439476233: Neon - Responsive Admin Template created by Laborator -->
</head>

<body class="page-body login-page login-form-fall">
<div class="login-container">
    <div class="login-header login-caret">
        <div class="login-content"> <a href="../../dashboard/main/index.html" class="logo">
                <img src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/images/logo@2x.png" width="120" alt="" /> </a>
            <p class="description">{$APP_DESCRIPTION}</p>
        </div>
    </div>

    <div class="login-form">
        <div class="login-content">
            {if $error|default:''}
                <div class="alert alert-danger">
                    <h3>Invalid login</h3>
                    {foreach from=$error item=err}
                        <p>{$err}</p>
                    {/foreach}
                </div>
            {/if}

            <form method="post" role="form" id="form_login" action="{$BASE_URL}login/">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"> <i class="entypo-user"></i>
                        </div>
                        <input type="text" class="form-control" name="username" id="username" placeholder="Username" autocomplete="off" />
                    </div>
                </div>

                <div class="form-group">

                    <div class="input-group">

                        <div class="input-group-addon"> <i class="entypo-key"></i>
                        </div>
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" />
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-login">
                        <i class="entypo-login"></i> Login In
                    </button>
                </div>
            </form>

            <div class="login-bottom-links"> <a href="#" class="link">Forgot your password?</a>
                <br /> <a href="#">{$APP_NAME}</a> - <a href="#">Privacy Policy</a>
            </div>
        </div>
    </div>
</div>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/gsap/main-gsap.js" id="script-resource-1"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js" id="script-resource-2"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/bootstrap.js" id="script-resource-3"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/joinable.js" id="script-resource-4"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/resizeable.js" id="script-resource-5"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/neon-api.js" id="script-resource-6"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/cookies.min.js" id="script-resource-7"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/jquery.validate.min.js" id="script-resource-8"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/neon-login.js" id="script-resource-9"></script>
<!-- JavaScripts initializations and stuff -->
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/neon-custom.js" id="script-resource-10"></script>
</body>
</html>