{include file="header.tpl"}
<body class="page-body page-fade gray">
<div class="page-container">
    {include file="side-bar.tpl"}

    <div class="main-content">
        {include file="top-bar.tpl"}

        <div class="row">
            <div class="col-md-12">

                <div class="well well-sm">
                    <h4><i class="entypo-layout"></i> Update Configuration</h4>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-body">

                        <div class="alert alert-info">
                            <p>Hi, be careful updating information on this page, it could affect the efficient operation of this platform.</p>
                        </div>

                        {if $error|default:''}
                            <div class="alert alert-warning">
                                <h3>Opps! No Update was made</h3>
                                {foreach from=$error item=err}
                                    <p>{$err}</p>
                                {/foreach}
                            </div>
                        {/if}

                        {if $okay|default:''}
                            <div class="alert alert-success">
                                <h3>Hey! Success Config. Update</h3>
                                <p>{$okay}</p>
                            </div>
                        {/if}

                        <form role="form" id="form1" action="{$BASE_URL}configuration/index/" method="post" class="validate">
                            {foreach from=$config_parameter item=config key=ek}
                                <div class="form-group">
                                    <label class="control-label">{$config.label|lower|capitalize|replace:'_':' '}
                                        <small class="text-muted">[{$config.info_type}]</small>
                                    </label>
                                    <input type="{$config.field_type}" class="form-control" name="{$config.label|lower}" data-validate="required" value="{$config.value}" {if $config.readonly_status}readonly="readonly"{/if} required="" />
                                </div>
                            {/foreach}
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-success"><i class="entypo-upload-cloud"></i> Update Configuration</button>
                            </div>
                        </form>
                    </div>
                </div>


            </div>
        </div>
        {include file="bottom-bar.tpl"}
    </div>
{include file="footer.tpl"}