
<!-- footer start -->
<footer class="footer section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">

                <!-- footer links and copyright -->
                <ul class="ft-links">
                    <li><a href="#" title="Terms & Conditions">Terms and Conditions</a></li>
                    <li><a href="#" title="Get Support">Support</a></li>
                </ul>
                <p class="copyright">&copy; 2015 {$APP_NAME}. All Rights Reserved.<br>Powered by Terragon Group.</p>

            </div>
            <div class="col-sm-6">

                <!-- social share links -->
                <ul class="social text-right">
                    <li><a href="javascript:void(0);" title="facebook" class="tool-tip"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="javascript:void(0);" title="twitter" class="tool-tip"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="javascript:void(0);" title="linkedin" class="tool-tip"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="javascript:void(0);" title="dribbble" class="tool-tip"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="javascript:void(0);" title="pinterest" class="tool-tip"><i class="fa fa-pinterest"></i></a></li>
                </ul>
                <!-- social share links -->

            </div>
        </div>
    </div>
</footer>
<!-- footer end -->


<!-- javascript/jquery plugins -->
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/vendor/jquery.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/vendor/bootstrap.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/matchMedia.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/smooth-scroll.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/smoothscroll.min.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/fitvids.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/backgroundvideo.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/nivo-lightbox.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/owl.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/retina.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/scroll-to-top.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/wow.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/form.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/validate.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/contact.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/ajaxchimp.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/classie.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/portfolio.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/projects.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/main.js"></script>
</body>

</html>