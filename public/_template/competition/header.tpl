<!--<h2>Hello Landing Page</h2>
<a href="{$tw_login_url}">Login with Twitter.....</a>-->
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>{$APP_NAME}</title>
    <meta name="description" content="{$APP_DESCRIPTION}">
    <meta name="keywords" content="{$APP_DESCRIPTION}">
    <meta name="author" content="Oluwasegun Matthew">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/css/main.css">
    <link id="themeColor" rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/css/color-skins/skin-15/color.css">

    <!-- modernizr -->
    <script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>

<body class="bg-img overlay-pattern {if $CURRENT_ACTION != "welcome"}no-top-pad{/if}" data-spy="scroll" data-target=".navscroll">


<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->


<!-- preloader animation start -->
<div class="preloader">
    <div class="wrap go">
        <div class="loader orbit">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
</div>
<!-- preloader animation end -->


<!-- top navigation start -->
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navscroll">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="#home" title="{$APP_NAME}">
                <img src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/img/premier-betting-auction-dark-logo.png" width="180" height="40" alt="{$APP_NAME}"></a>
        </div>
        <div class="navbar-collapse collapse navscroll">
            {if $tw_login_url|default:''}
            <div class="navbar-form navbar-right">
                <a href="{$tw_login_url}" title="Participate in {$APP_NAME}" class="btn btn-block btn-default download-link">
                    <i class="icon_cloud-download_alt"></i> Join Auction!
                </a>
            </div>
            {/if}
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{$BASE_URL}competition/welcome/#welcome">Welcome</a></li>
                <li><a href="{$BASE_URL}competition/welcome/#about-competition">About Competition</a></li>
                {if $participants|default:''}
                    <li><a href="{$BASE_URL}competition/welcome/#participants">Participants</a></li>
                {/if}
                <!--<li><a href="#team">Team</a></li>
                <li><a href="#pricing">Pricing</a></li>
                <li><a href="#contact">Contact</a></li>
                -->
            </ul>
        </div>
        <!--/.navbar-collapse -->
    </div>
</div>
<!-- top navigation end -->