{include file="competition/header.tpl"}
<div class="feature-list section">
    <div class="container">
        <div class="row">
            <!--<div class="col-md-2 col-sm-2 col-xs-2">
                <div class="app-layers text-center">

                </div>

            </div>-->
            <div class="col-lg-12  wow fadeInRight" data-wow-delay="1s">
                <img class="profile-pic-cloth" src="{$participant_profile.profile_image_url}" alt="user profile">
                <h2 class="h1 profile-head">{$participant_profile.name}</h2>
                <p class="text-left">{$participant_profile.description}</p>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<hr class="line">
<!-- contact and subscription forms start -->
<div class="forms" id="finish">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <!-- contact form start -->
                <div class="contact-form" style="display:block;">
                    <h2 class="h1 text-center">Complete your Profile <small>Provide accurate information</small></h2>

                    {if $error|default:''}
                        <div class="alert alert-danger">
                        {foreach from=$error item=err}
                            <p>* {$err}</p>
                        {/foreach}
                        </div>
                    {/if}

                    <form method="post" action="{$BASE_URL}competition/finish/" method="post" role="form" >
                        <div class="row">

                            <div class="col-lg-5">
                                <div class="form-group">
                                    <input type="email" class="form-control input-lg" name="email" placeholder="Email Address" required>
                                </div>
                            </div>

                            <div class="col-lg-5 col-lg-offset-1">
                                <div class="form-group">
                                    <input type="text" class="form-control input-lg" name="phone" placeholder="Your Phone Number" required>
                                </div>
                            </div>


                            <div class="col-lg-5">
                                <div class="form-group">
                                   <select name="gender" class="form-control input-lg" required>
                                       <option value="">Choose Gender</option>
                                       <option value="Male">Male</option>
                                       <option value="Female">Female</option>
                                   </select>
                                </div>
                            </div>

                            <div class="col-lg-5 col-lg-offset-1">
                                <div class="form-group">
                                    <input type="number" class="form-control input-lg" name="age" placeholder="Age" required>
                                </div>
                            </div>


                            <div class="col-lg-11">
                                <div class="form-group">
                                    <select name="state" class="form-control input-lg" required>
                                        <option value="" selected="selected">Choose State of Origin</option>
                                        <option value="Abuja FCT">Abuja FCT</option>
                                        <option value="Abia">Abia</option>
                                        <option value="Adamawa">Adamawa</option>
                                        <option value="Akwa Ibom">Akwa Ibom</option>
                                        <option value="Anambra">Anambra</option>
                                        <option value="Bauchi">Bauchi</option>
                                        <option value="Bayelsa">Bayelsa</option>
                                        <option value="Benue">Benue</option>
                                        <option value="Borno">Borno</option>
                                        <option value="Cross River">Cross River</option>
                                        <option value="Delta">Delta</option>
                                        <option value="Ebonyi">Ebonyi</option>
                                        <option value="Edo">Edo</option>
                                        <option value="Ekiti">Ekiti</option>
                                        <option value="Enugu">Enugu</option>
                                        <option value="Gombe">Gombe</option>
                                        <option value="Imo">Imo</option>
                                        <option value="Jigawa">Jigawa</option>
                                        <option value="Kaduna">Kaduna</option>
                                        <option value="Kano">Kano</option>
                                        <option value="Katsina">Katsina</option>
                                        <option value="Kebbi">Kebbi</option>
                                        <option value="Kogi">Kogi</option>
                                        <option value="Kwara">Kwara</option>
                                        <option value="Lagos">Lagos</option>
                                        <option value="Nassarawa">Nassarawa</option>
                                        <option value="Niger">Niger</option>
                                        <option value="Ogun">Ogun</option>
                                        <option value="Ondo">Ondo</option>
                                        <option value="Osun">Osun</option>
                                        <option value="Oyo">Oyo</option>
                                        <option value="Plateau">Plateau</option>
                                        <option value="Rivers">Rivers</option>
                                        <option value="Sokoto">Sokoto</option>
                                        <option value="Taraba">Taraba</option>
                                        <option value="Yobe">Yobe</option>
                                        <option value="Zamfara">Zamfara</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-11">
                                <div class="form-group text-right">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block text-center">Update Profile &nbsp; Enter Competition</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- contact form end -->

                <!-- form switch buttons start -->
                <div class="form-switch">
                    <button class="contact-btn active tool-tip" id="onpen-contact-form">
                        <i class="fa fa-cloud-upload"></i>
                    </button>
                </div>
                <!-- form switch buttons end -->

            </div>
        </div>
    </div>
</div>
<!-- contact and subscription forms start -->
{include file="competition/footer.tpl"}