<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="Laborator.co" />
    <title>{$APP_NAME}</title>

    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css" id="style-resource-1">
    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/css/font-icons/entypo/css/entypo.css">
    <link href='http://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/css/bootstrap.css" id="style-resource-4">
    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/css/neon-core.css" id="style-resource-5">
    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/css/neon-theme.css" id="style-resource-6">
    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/css/neon-forms.css" id="style-resource-7">
    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/css/custom.css" id="style-resource-8">
    <script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/jquery-1.11.0.min.js"></script>
    <script>$.noConflict();</script>
    <!--[if lt IE 9]>
    <script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script> <![endif]-->
</head>