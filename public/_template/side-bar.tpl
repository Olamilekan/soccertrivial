<div class="sidebar-menu">
    <div class="sidebar-menu-inner">
        <header class="logo-env">
            <!-- logo -->
            <div class="logo">
                <a href="{$BASE_URL}dashboard/"> <img src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/images/logo@2x.png" width="120" alt="" /> </a>
            </div>
            <!-- logo collapse icon -->
            <div class="sidebar-collapse">
                <a href="#" class="sidebar-collapse-icon">
                <i class="entypo-menu"></i> </a>
            </div>
            <div class="sidebar-mobile-menu visible-xs">
                <a href="#" class="with-animation">
                <i class="entypo-menu"></i> </a>
            </div>
        </header>

        <ul id="main-menu" class="main-menu">
            <li> <a href="{$BASE_URL}dashboard/" target="_blank">
                    <i class="entypo-gauge"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>

            <li {if $CURRENT_PAGE == 'targets'}class="opened"{/if}>
                <a href="#">
                    <i class="entypo-layout"></i>
                    <span class="title">Targets</span>
                    <span class="badge badge-success">{$synced_target|default:0}</span>
                </a>
                <ul>
                    <li> <a href="{$BASE_URL}targets/create/">
                            <span class="title">Create Target</span>
                        </a>
                    </li>

                    <li> <a href="{$BASE_URL}targets/view/">
                            <span class="title">View Targets</span>
                        </a>
                    </li>

                    <li> <a href="{$BASE_URL}targets/view-thumbnails/">
                            <span class="title">Targets Thumbnails</span>
                        </a>
                    </li>

                    <li> <a href="{$BASE_URL}targets/report/">
                            <span class="title">Reports on Target's Campaign</span>
                        </a>
                    </li>
                </ul>
            </li>



            <li>
                <a href="#">
                    <i class="entypo-window"></i>
                    <span class="title">Campaigns</span>
                </a>
                <ul>
                    <li> <a href="{$BASE_URL}campaign/create/">
                            <span class="title">Create a Campaign</span>
                        </a>
                    </li>

                    <li> <a href="{$BASE_URL}campaign/view/">
                            <span class="title">List of Campaigns</span>
                        </a>
                    </li>
                </ul>
            </li>


            <li {if $CURRENT_PAGE == 'account'}class="opened"{/if}>
                <a href="#">
                    <i class="entypo-user"></i>
                    <span class="title">Account</span>
                    <span class="badge badge-info badge-roundless">New Feature</span>
                </a>
                <ul>
                    <li> <a href="{$BASE_URL}account/create/">
                            <span class="title">Create New Account</span>
                        </a>
                    </li>

                    <li> <a href="{$BASE_URL}account/view/">
                            <span class="title">List of Accounts</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li> <a href="{$BASE_URL}configuration/">
                    <i class="entypo-cog"></i>
                    <span class="title">Configuration</span>
                </a>
            </li>

        </ul>
    </div>
</div>