<?php /* Smarty version 3.1.24, created on 2016-05-10 13:17:00
         compiled from "public/_template/competition/header.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:8598375205731d13c8f5766_82657246%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ba049cd803a10518caf0c7cfae5862f4f868c624' => 
    array (
      0 => 'public/_template/competition/header.tpl',
      1 => 1462787437,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8598375205731d13c8f5766_82657246',
  'variables' => 
  array (
    'tw_login_url' => 0,
    'APP_NAME' => 0,
    'APP_DESCRIPTION' => 0,
    'BASE_URL' => 0,
    'SMARTY_VIEW_FOLDER' => 0,
    'CURRENT_ACTION' => 0,
    'participants' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_5731d13c8fdc99_90169845',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5731d13c8fdc99_90169845')) {
function content_5731d13c8fdc99_90169845 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '8598375205731d13c8f5766_82657246';
?>
<!--<h2>Hello Landing Page</h2>
<a href="<?php echo $_smarty_tpl->tpl_vars['tw_login_url']->value;?>
">Login with Twitter.....</a>-->
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
</title>
    <meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['APP_DESCRIPTION']->value;?>
">
    <meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['APP_DESCRIPTION']->value;?>
">
    <meta name="author" content="Oluwasegun Matthew">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/css/main.css">
    <link id="themeColor" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/css/color-skins/skin-15/color.css">

    <!-- modernizr -->
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"><?php echo '</script'; ?>
>
</head>

<body class="bg-img overlay-pattern <?php if ($_smarty_tpl->tpl_vars['CURRENT_ACTION']->value != "welcome") {?>no-top-pad<?php }?>" data-spy="scroll" data-target=".navscroll">


<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->


<!-- preloader animation start -->
<div class="preloader">
    <div class="wrap go">
        <div class="loader orbit">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
</div>
<!-- preloader animation end -->


<!-- top navigation start -->
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navscroll">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="#home" title="<?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
">
                <img src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/img/premier-betting-auction-dark-logo.png" width="180" height="40" alt="<?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
"></a>
        </div>
        <div class="navbar-collapse collapse navscroll">
            <?php if ((($tmp = @$_smarty_tpl->tpl_vars['tw_login_url']->value)===null||$tmp==='' ? '' : $tmp)) {?>
            <div class="navbar-form navbar-right">
                <a href="<?php echo $_smarty_tpl->tpl_vars['tw_login_url']->value;?>
" title="Participate in <?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
" class="btn btn-block btn-default download-link">
                    <i class="icon_cloud-download_alt"></i> Join Auction!
                </a>
            </div>
            <?php }?>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
competition/welcome/#welcome">Welcome</a></li>
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
competition/welcome/#about-competition">About Competition</a></li>
                <?php if ((($tmp = @$_smarty_tpl->tpl_vars['participants']->value)===null||$tmp==='' ? '' : $tmp)) {?>
                    <li><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
competition/welcome/#participants">Participants</a></li>
                <?php }?>
                <!--<li><a href="#team">Team</a></li>
                <li><a href="#pricing">Pricing</a></li>
                <li><a href="#contact">Contact</a></li>
                -->
            </ul>
        </div>
        <!--/.navbar-collapse -->
    </div>
</div>
<!-- top navigation end --><?php }
}
?>