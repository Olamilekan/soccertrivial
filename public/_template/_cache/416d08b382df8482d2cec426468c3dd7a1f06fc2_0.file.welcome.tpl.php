<?php /* Smarty version 3.1.24, created on 2016-05-10 13:17:00
         compiled from "public/_template/competition/welcome.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:8443831195731d13c893221_43122642%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '416d08b382df8482d2cec426468c3dd7a1f06fc2' => 
    array (
      0 => 'public/_template/competition/welcome.tpl',
      1 => 1462787437,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8443831195731d13c893221_43122642',
  'variables' => 
  array (
    'BASE_URL' => 0,
    'SMARTY_VIEW_FOLDER' => 0,
    'APP_NAME' => 0,
    'participant_profile' => 0,
    'campaign_url' => 0,
    'tw_login_url' => 0,
    'SEARCH_QUERY' => 0,
    'participants' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_5731d13c8f0569_72392674',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5731d13c8f0569_72392674')) {
function content_5731d13c8f0569_72392674 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '8443831195731d13c893221_43122642';
echo $_smarty_tpl->getSubTemplate ("competition/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<!-- main banner start -->
<div class="banner" id="welcome">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-sm-7">

                <!-- logo, main heading, CTA buttons  -->
                <a href="#home" class="logo" title="Obsession"><img src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/img/premier-betting-auction-logo.png" width="250" alt="<?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
"></a>
                <?php if ((($tmp = @$_smarty_tpl->tpl_vars['participant_profile']->value)===null||$tmp==='' ? '' : $tmp)) {?>
                    <h1>
                        Hi <strong><?php echo $_smarty_tpl->tpl_vars['participant_profile']->value['name'];?>
</strong>,
                        <?php if ($_smarty_tpl->tpl_vars['participant_profile']->value['complete_status']) {?>
                            It's time to Win Smart.
                            <small>Click the link below to place quick bet on...</small>
                        <?php } else { ?>
                            Only complete profile is allowed in the competition
                            <small>Kindly update your profile below...</small>
                        <?php }?>
                    </h1>
                <?php } else { ?>
                    <h1>
                        Do you want to be <strong>Recognized</strong> and <strong>Rewarded</strong> again?
                        <small>Then Partake of <?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
</small>
                    </h1>
                <?php }?>
                <div class="cta-btns">
                    <?php if ((($tmp = @$_smarty_tpl->tpl_vars['participant_profile']->value)===null||$tmp==='' ? '' : $tmp)) {?>
                        <?php if ($_smarty_tpl->tpl_vars['participant_profile']->value['complete_status']) {?>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['campaign_url']->value;?>
" class="btn btn-primary btn-lg download-link" title="Go Win via <?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
">
                                <i class="fa fa-trophy"></i> Go Win a Bet!
                            </a>
                        <?php } else { ?>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
competition/finish/" class="btn btn-primary btn-lg download-link" title="Update Profile on <?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
">
                                <i class="fa fa-cloud-upload"></i> Update Profile
                            </a>
                        <?php }?>
                    <?php } else { ?>
                        <?php if ((($tmp = @$_smarty_tpl->tpl_vars['tw_login_url']->value)===null||$tmp==='' ? '' : $tmp)) {?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['tw_login_url']->value;?>
" class="btn btn-primary btn-lg download-link" title="Participate in <?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
">
                            <i class="icon_cloud-download_alt"></i> Join Auction
                        </a>
                        <?php }?>
                    <?php }?>
                    <a href="#about-competition" class="btn btn-primary btn-lg btn-link learn-more" title="Learn more about <?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
">
                        Learn More <i class="fa fa-angle-double-down"></i>
                    </a>
                </div>

            </div>
            <div class="col-sm-5 hidden-xs">
                <div class="phone-mockup style1">
                    <div class="wrapper">
                        <img class="layer-one wow fadeInLeft visible-lg" data-wow-delay="1s"
                             src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/img/mockups/iphone-dark.png" width="316" height="581">
                        <img class="layer-two" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/img/mockups/iphone-white.png"
                             width="316" height="581">
                    </div>
                </div>
                <!-- iPhone mockup style1 end -->
            </div>
        </div>

        <!-- available on -->
        <small class="available-on"><?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
 | Terms and Conditions Apply.</small>

    </div>
</div>
<!-- main banner end -->


<!-- about-compeitition section start -->
<div class="features section" id="about-competition">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2 class="h2 text-center"><?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
</h2>
                <hr>
                <p class="lead text-center">Another opportunity to "BET" with ease and get guaranteed "WINNING".<br> Go through steps below to make your win come through....</p>
            </div>
        </div>

        <!-- spacer -->
        <hr class="spacer">

        <div class="row">
            <div class="col-md-6">

                <!-- feature style2 -->
                <div class="feature style3 wow fadeInUp" data-wow-delay="1s">
                    <i class="icon_cloud-download_alt"></i>
                    <h4>Join Auction</h4>
                    <p>Using your Twitter Social Media account. All that is required is for your approval at authentication level, only basic information will be retrieve off your account.</p>
                </div>
                <!-- feature style1 -->

            </div>
            <div class="col-md-6">

                <!-- feature style2 -->
                <div class="feature style3 wow fadeInUp" data-wow-delay="1.2s">
                    <i class="icon_search-2"></i>
                    <h4>Update Profile</h4>
                    <p>Provide some other information to fully opt into competition, afterwards, you shall b eredirected to the betting platform</p>
                </div>
                <!-- feature style1 -->
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">

                <!-- feature style2 -->
                <div class="feature style3 wow fadeInUp" data-wow-delay="1s">
                    <i class="icon-globe-alt"></i>
                    <h4>Continuous Tweet and Retweet</h4>
                    <p>As you run your bet, continous tweet and retweet (in a creative way) about hashtag <strong><?php echo $_smarty_tpl->tpl_vars['SEARCH_QUERY']->value;?>
</strong> on your twitter handle to give you winning leverage. </p>
                </div>
                <!-- feature style1 -->

            </div>
            <div class="col-md-6">

                <!-- feature style2 -->
                <div class="feature style3 wow fadeInUp" data-wow-delay="1.2s">
                    <i class="icon-equalizer"></i>
                    <h4>Monitor Progress</h4>
                    <p>Check our<strong><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
competition/dashboard/"> Dashboard</a></strong> regularly to know when to up your game on your twitter timeline. Only continous tweet and retweet about hashtag  <strong><?php echo $_smarty_tpl->tpl_vars['SEARCH_QUERY']->value;?>
</strong> can keep you in the competition.</p>
                </div>
                <!-- feature style1 -->

            </div>
        </div>
    </div>
</div>
<!-- about-competition section end -->

<hr class="line">

<?php if ((($tmp = @$_smarty_tpl->tpl_vars['participants']->value)===null||$tmp==='' ? '' : $tmp)) {?>
<!-- participants section start -->
<div class="team-container section" id="participants">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="h1 text-center">Our awesome little team <small>Marc & Ben worked hard together and came up with an amazing app</small></h2>

                <!-- spacer -->
                <hr class="spacer">

                <div class="row">
                    <div class="col-lg-6">

                        <!-- team member left carousel start -->
                        <div class="owl-carousel" id="team-left">
                            <div class="team-member left">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/img/team/1.jpg" title="Ben Smith" class="lightbox"><img class="member-photo" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/img/team/1.jpg" width="768" height="862" alt="Ben Smith"></a>
                                <div class="content">
                                    <h3 class="member-desig"><span>Designer</span> Ben Smith</h3>
                                    <p class="member-detail">Ben is lead graphic designer and have designed more than 200 stunning app s since he joined us.</p>
                                    <div class="social">
                                        <a href="javascript:void(0);" class="tool-tip" title="Dribbble"><i class="fa fa-dribbble"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="Facebook"><i class="fa fa-facebook"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="LinkedIn"><i class="fa fa fa-linkedin"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="Twitter"><i class="fa fa-twitter"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="team-member left">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/img/team/3.jpg" title="Ben Smith" class="lightbox"><img class="member-photo" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/img/team/3.jpg" width="768" height="862" alt="Ben Smith"></a>
                                <div class="content">
                                    <h3 class="member-desig"><span>UI/UX</span> Daniel Chaudhry</h3>
                                    <p class="member-detail">Daniel is an awesome guy. He is UI/Ux expert and our right hand. He smokes too much as you can see here.</p>
                                    <div class="social">
                                        <a href="javascript:void(0);" class="tool-tip" title="Dribbble"><i class="fa fa-dribbble"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="Facebook"><i class="fa fa-facebook"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="LinkedIn"><i class="fa fa fa-linkedin"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="Twitter"><i class="fa fa-twitter"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- team member left carousel end -->

                    </div>
                    <div class="col-lg-6">

                        <!-- team member right carousel start -->
                        <div class="owl-carousel" id="team-right">
                            <div class="team-member right">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/img/team/2.jpg" title="Ben Smith" class="lightbox"><img class="member-photo" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/img/team/2.jpg" width="768" height="862" alt="Ben Smith"></a>
                                <div class="content">
                                    <h3 class="member-desig"><span>Developer</span> Marc Eddy</h3>
                                    <p class="member-detail">Marc is founder and back-end developer. He started development in 1998 and still a developer, because he loves development.</p>
                                    <div class="social">
                                        <a href="javascript:void(0);" class="tool-tip" title="Dribbble"><i class="fa fa-dribbble"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="Facebook"><i class="fa fa-facebook"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="LinkedIn"><i class="fa fa fa-linkedin"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="Twitter"><i class="fa fa-twitter"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="team-member right">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/img/team/4.jpg" title="Ben Smith" class="lightbox"><img class="member-photo" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/img/team/4.jpg" width="768" height="862" alt="Ben Smith"></a>
                                <div class="content">
                                    <h3 class="member-desig"><span>QA</span> Shan Mughal</h3>
                                    <p class="member-detail">Shan is our quality assurance expert. He has 10 yeras of enterprise level applications deployment experience.</p>
                                    <div class="social">
                                        <a href="javascript:void(0);" class="tool-tip" title="Dribbble"><i class="fa fa-dribbble"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="Facebook"><i class="fa fa-facebook"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="LinkedIn"><i class="fa fa fa-linkedin"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="Twitter"><i class="fa fa-twitter"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- team member right carousel end -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- participants section start -->
<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ("competition/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>