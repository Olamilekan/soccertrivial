<?php /* Smarty version 3.1.24, created on 2016-05-10 13:17:00
         compiled from "public/_template/competition/footer.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:17743814685731d13c9031c5_77538948%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'daf7cb9bb340ac3b18b76609a1cafdfc0bfb4fbe' => 
    array (
      0 => 'public/_template/competition/footer.tpl',
      1 => 1462787437,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17743814685731d13c9031c5_77538948',
  'variables' => 
  array (
    'APP_NAME' => 0,
    'BASE_URL' => 0,
    'SMARTY_VIEW_FOLDER' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_5731d13c90b681_35818054',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5731d13c90b681_35818054')) {
function content_5731d13c90b681_35818054 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '17743814685731d13c9031c5_77538948';
?>

<!-- footer start -->
<footer class="footer section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">

                <!-- footer links and copyright -->
                <ul class="ft-links">
                    <li><a href="#" title="Terms & Conditions">Terms and Conditions</a></li>
                    <li><a href="#" title="Get Support">Support</a></li>
                </ul>
                <p class="copyright">&copy; 2015 <?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
. All Rights Reserved.<br>Powered by Terragon Group.</p>

            </div>
            <div class="col-sm-6">

                <!-- social share links -->
                <ul class="social text-right">
                    <li><a href="javascript:void(0);" title="facebook" class="tool-tip"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="javascript:void(0);" title="twitter" class="tool-tip"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="javascript:void(0);" title="linkedin" class="tool-tip"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="javascript:void(0);" title="dribbble" class="tool-tip"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="javascript:void(0);" title="pinterest" class="tool-tip"><i class="fa fa-pinterest"></i></a></li>
                </ul>
                <!-- social share links -->

            </div>
        </div>
    </div>
</footer>
<!-- footer end -->


<!-- javascript/jquery plugins -->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/vendor/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/vendor/bootstrap.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/matchMedia.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/smooth-scroll.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/smoothscroll.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/fitvids.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/backgroundvideo.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/nivo-lightbox.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/owl.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/retina.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/scroll-to-top.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/wow.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/validate.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/contact.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/ajaxchimp.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/classie.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/portfolio.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/projects.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/competition/js/main.js"><?php echo '</script'; ?>
>
</body>

</html><?php }
}
?>