{include file="header.tpl"}
<body class="page-body page-fade gray">
<div class="page-container">
    {include file="side-bar.tpl"}

    <div class="main-content">
        {include file="top-bar.tpl"}

        <div class="row">
            <div class="col-md-12">

                <div class="well well-sm">
                    <h4><i class="entypo-layout"></i> Targets in Thumbnails
                        <p class="pull-right">
                            <a href="{$BASE_URL}targets/view/"><i class="entypo-layout"></i></a>
                        </p>
                    </h4>
                </div>



                {literal}
                    <script type="text/javascript">
                        jQuery(document).ready(function($) {
                            /*$(".gallery-env").on("click", ".image-thumb .image-options a.delete", function(ev) {
                                ev.preventDefault();

                                var $image = $(this).closest('[data-tag]');
                                var t = new TimelineLite({
                                    onComplete: function() {
                                        $image.slideUp(function() {
                                            $image.remove();
                                        });
                                    }
                                });
                                $image.addClass('no-animation');
                                t.append(TweenMax.to($image, .2, {
                                    css: {
                                        scale: 0.95
                                    }
                                }));
                                t.append(TweenMax.to($image, .5, {
                                    css: {
                                        autoAlpha: 0,
                                        transform: "translateX(100px) scale(.95)"
                                    }
                                }));
                            }).on("click", ".image-thumb .image-options a.edit", function(ev) {
                                ev.preventDefault();
                                // This will open sample modal
                                $("#album-image-options").modal('show');
                                // Sample Crop Instance
                                var image_to_crop = $("#album-image-options img"),
                                        img_load = new Image();
                                img_load.src = image_to_crop.attr('src');
                                img_load.onload = function() {
                                    if (image_to_crop.data('loaded'))
                                        return false;
                                    image_to_crop.data('loaded', true);
                                    image_to_crop.Jcrop({
                                        //boxWidth: $("#album-image-options").outerWidth(),
                                        boxWidth: 580,
                                        boxHeight: 385,
                                        onSelect: function(cords) {
                                            // you can use these vars to save cropping of the image coordinates
                                            var h = cords.h,
                                                    w = cords.w,
                                                    x1 = cords.x,
                                                    x2 = cords.x2,
                                                    y1 = cords.w,
                                                    y2 = cords.y2;
                                        }
                                    }, function() {
                                        var jcrop = this;
                                        jcrop.animateTo([600, 400, 100, 150]);
                                    });
                                }
                            });*/

                            // Sample Filtering
                            var all_items = $("div[data-tag]"),
                                    categories_links = $(".image-categories a");
                            categories_links.click(function(ev) {
                                ev.preventDefault();
                                var $this = $(this),
                                        filter = $this.data('filter');
                                categories_links.removeClass('active');
                                $this.addClass('active');
                                all_items.addClass('not-in-filter').filter('[data-tag="' + filter + '"]').removeClass('not-in-filter');
                                if (filter == 'all' || filter == '*') {
                                    all_items.removeClass('not-in-filter');
                                    return;
                                }
                            });
                        });
                    </script>
                {/literal}

                <div class="gallery-env">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="image-categories"> <span>Filter Images:</span>
                                <a href="#" class="active" data-filter="all">Show All</a> /
                                <a href="#" data-filter="sync">Synced</a> /
                                <a href="#" data-filter="non-sync">Pending Sync.</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        {foreach from=$target_list item=target key=ek}
                        <div class="col-sm-2 col-xs-4" data-tag="{if $target.sync_flag}sync{else}non-sync{/if}">
                            <article class="image-thumb">
                                <a href="#" class="image">
                                    <img src="{$TARGET_URL}{$target.image_path}" />
                                </a>
                                <div class="image-options">
                                    <a href="{$BASE_URL}targets/edit/{$target.target_id}/modify/" class="edit">
                                        <i class="entypo-pencil"></i>
                                    </a>
                                    <a href="{$BASE_URL}targets/edit/{$target.target_id}/delete/" class="delete">
                                        <i class="entypo-cancel"></i>
                                    </a>
                                </div>
                            </article>
                        </div>
                        {/foreach}

                        <!--
                        <div class="col-sm-2 col-xs-4" data-tag="3d">
                            <article class="image-thumb">
                                <a href="index.html#" class="image">
                                    <img src="../../assets/images/album-image-2.jpg" />
                                </a>
                                <div class="image-options"> <a href="index.html#" class="edit"><i class="entypo-pencil"></i></a>  <a href="index.html#" class="delete"><i class="entypo-cancel"></i></a>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-2 col-xs-4" data-tag="3d">
                            <article class="image-thumb">
                                <a href="index.html#" class="image">
                                    <img src="../../assets/images/album-image-3.jpg" />
                                </a>
                                <div class="image-options"> <a href="index.html#" class="edit"><i class="entypo-pencil"></i></a>  <a href="index.html#" class="delete"><i class="entypo-cancel"></i></a>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-2 col-xs-4" data-tag="1d">
                            <article class="image-thumb">
                                <a href="index.html#" class="image">
                                    <img src="../../assets/images/album-image-4.jpg" />
                                </a>
                                <div class="image-options"> <a href="index.html#" class="edit"><i class="entypo-pencil"></i></a>  <a href="index.html#" class="delete"><i class="entypo-cancel"></i></a>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-2 col-xs-4" data-tag="1d">
                            <article class="image-thumb">
                                <a href="index.html#" class="image">
                                    <img src="../../assets/images/album-image-5.jpg" />
                                </a>
                                <div class="image-options"> <a href="index.html#" class="edit"><i class="entypo-pencil"></i></a>  <a href="index.html#" class="delete"><i class="entypo-cancel"></i></a>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-2 col-xs-4" data-tag="3d">
                            <article class="image-thumb">
                                <a href="index.html#" class="image">
                                    <img src="../../assets/images/album-image-6.jpg" />
                                </a>
                                <div class="image-options"> <a href="index.html#" class="edit"><i class="entypo-pencil"></i></a>  <a href="index.html#" class="delete"><i class="entypo-cancel"></i></a>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-2 col-xs-4" data-tag="1w">
                            <article class="image-thumb">
                                <a href="index.html#" class="image">
                                    <img src="../../assets/images/album-image-7.jpg" />
                                </a>
                                <div class="image-options"> <a href="index.html#" class="edit"><i class="entypo-pencil"></i></a>  <a href="index.html#" class="delete"><i class="entypo-cancel"></i></a>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-2 col-xs-4" data-tag="1d">
                            <article class="image-thumb">
                                <a href="index.html#" class="image">
                                    <img src="../../assets/images/album-image-8.jpg" />
                                </a>
                                <div class="image-options"> <a href="index.html#" class="edit"><i class="entypo-pencil"></i></a>  <a href="index.html#" class="delete"><i class="entypo-cancel"></i></a>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-2 col-xs-4" data-tag="1w">
                            <article class="image-thumb">
                                <a href="index.html#" class="image">
                                    <img src="../../assets/images/album-image-9.jpg" />
                                </a>
                                <div class="image-options"> <a href="index.html#" class="edit"><i class="entypo-pencil"></i></a>  <a href="index.html#" class="delete"><i class="entypo-cancel"></i></a>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-2 col-xs-4" data-tag="1w">
                            <article class="image-thumb">
                                <a href="index.html#" class="image">
                                    <img src="../../assets/images/album-image-10.jpg" />
                                </a>
                                <div class="image-options"> <a href="index.html#" class="edit"><i class="entypo-pencil"></i></a>  <a href="index.html#" class="delete"><i class="entypo-cancel"></i></a>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-2 col-xs-4" data-tag="3d">
                            <article class="image-thumb">
                                <a href="index.html#" class="image">
                                    <img src="../../assets/images/album-image-11.jpg" />
                                </a>
                                <div class="image-options"> <a href="index.html#" class="edit"><i class="entypo-pencil"></i></a>  <a href="index.html#" class="delete"><i class="entypo-cancel"></i></a>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-2 col-xs-4" data-tag="1w">
                            <article class="image-thumb">
                                <a href="index.html#" class="image">
                                    <img src="../../assets/images/album-image-12.jpg" />
                                </a>
                                <div class="image-options"> <a href="index.html#" class="edit"><i class="entypo-pencil"></i></a>  <a href="index.html#" class="delete"><i class="entypo-cancel"></i></a>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-2 col-xs-4" data-tag="1d">
                            <article class="image-thumb">
                                <a href="index.html#" class="image">
                                    <img src="../../assets/images/album-image-13.jpg" />
                                </a>
                                <div class="image-options"> <a href="index.html#" class="edit"><i class="entypo-pencil"></i></a>  <a href="index.html#" class="delete"><i class="entypo-cancel"></i></a>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-2 col-xs-4" data-tag="1w">
                            <article class="image-thumb">
                                <a href="index.html#" class="image">
                                    <img src="../../assets/images/album-image-14.jpg" />
                                </a>
                                <div class="image-options"> <a href="index.html#" class="edit"><i class="entypo-pencil"></i></a>  <a href="index.html#" class="delete"><i class="entypo-cancel"></i></a>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-2 col-xs-4" data-tag="1d">
                            <article class="image-thumb">
                                <a href="index.html#" class="image">
                                    <img src="../../assets/images/album-image-15.jpg" />
                                </a>
                                <div class="image-options"> <a href="index.html#" class="edit"><i class="entypo-pencil"></i></a>  <a href="index.html#" class="delete"><i class="entypo-cancel"></i></a>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-2 col-xs-4" data-tag="3d">
                            <article class="image-thumb">
                                <a href="index.html#" class="image">
                                    <img src="../../assets/images/album-image-16.jpg" />
                                </a>
                                <div class="image-options"> <a href="index.html#" class="edit"><i class="entypo-pencil"></i></a>  <a href="index.html#" class="delete"><i class="entypo-cancel"></i></a>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-2 col-xs-4" data-tag="1w">
                            <article class="image-thumb">
                                <a href="index.html#" class="image">
                                    <img src="../../assets/images/album-image-17.jpg" />
                                </a>
                                <div class="image-options"> <a href="index.html#" class="edit"><i class="entypo-pencil"></i></a>  <a href="index.html#" class="delete"><i class="entypo-cancel"></i></a>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-2 col-xs-4" data-tag="1d">
                            <article class="image-thumb">
                                <a href="index.html#" class="image">
                                    <img src="../../assets/images/album-image-18.jpg" />
                                </a>
                                <div class="image-options"> <a href="index.html#" class="edit"><i class="entypo-pencil"></i></a>  <a href="index.html#" class="delete"><i class="entypo-cancel"></i></a>
                                </div>
                            </article>
                        </div>-->
                    </div>
                </div>


            </div>
        </div>
        {include file="bottom-bar.tpl"}
    </div>
{include file="footer.tpl"}