{include file="header.tpl"}
<body class="page-body page-fade gray">
<div class="page-container">
    {include file="side-bar.tpl"}

    <div class="main-content">
        {include file="top-bar.tpl"}

        <div class="row">
            <div class="col-md-12">

                <div class="well well-sm">
                    <h4><i class="entypo-layout"></i> List of Campaigns </h4>
                </div>

                <table class="table table-bordered table-striped datatable" id="table-2">
                    <thead>
                    <tr>
                        <th class="text-center">S/N</th>
                        <th class="text-center">Account Name</th>
                        <th class="text-center">Title</th>
                        <th class="text-center">Description</th>
                        <th class="text-center">Duration</th>
                        <th class="text-center">Date Created</th>
                        <th class="text-center">Actions</th>
                        <th class="text-center"></th>

                    </tr>
                    </thead>
                    <tbody>
                        {foreach from=$campaign_list item=account key=ek}
                            <tr>
                                <td class="text-center">{$ek+1}</td>
                                <td>{$account.name}</td>
                                <td>{$account.title}</td>
                                <td>{$account.description}</td>
                                <td class="text-center">{$account.duration}</td>
                                <td class="text-center">{$account.date_created}</td>
                                <td class="text-center">
                                    <a href="#" class="btn btn-default btn-xs"> <i class="entypo-pencil"></i></a>
                                    <a href="index.html#" class="btn btn-danger btn-xs">
                                        <i class="entypo-cancel"> </i>
                                    </a>
                                </td>
                                 <td class="text-center">
                                     {$account.campaign_id}
                                 </td>
                            </tr>
                        {/foreach}
                    </tbody>
                </table>

                <div class="text-right">
                    {if $paginate_data.links}
                        {$paginate_data.links}
                        <p>{*$paginate_data.page_message*}</p>
                    {/if}
                </div>

                {literal}
                <script type="text/javascript">
                    jQuery(window).load(function() {
                        var $ = jQuery;
                        $("#table-2").dataTable({
                            "sPaginationType": "bootstrap",
                            "sDom": "t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
                            "bStateSave": false,
                            "iDisplayLength": 20,
                            "aoColumns": [{
                                "bSortable": false
                            },
                                null,
                                null,
                                null,
                                null
                            ]
                        });
                        $(".dataTables_wrapper select").select2({
                            minimumResultsForSearch: -1
                        });
                        // Highlighted rows
                        $("#table-2 tbody input[type=checkbox]").each(function(i, el) {
                            var $this = $(el),
                                    $p = $this.closest('tr');
                            $(el).on('change', function() {
                                var is_checked = $this.is(':checked');
                                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
                            });
                        });
                        // Replace Checboxes
                        $(".pagination a").click(function(ev) {
                            replaceCheckboxes();
                        });
                    });
                    // Sample Function to add new row
                    var giCount = 1;

                    function fnClickAddRow() {
                        $('#table-2').dataTable().fnAddData(['<div class="checkbox checkbox-replace"><input type="checkbox" /></div>', giCount + ".2", giCount + ".3", giCount + ".4", giCount + ".5"]);
                        replaceCheckboxes(); // because there is checkbox, replace it
                        giCount++;
                    }
                </script>
                {/literal}
            </div>
        </div>
        {include file="bottom-bar.tpl"}
    </div>
{include file="footer.tpl"}