{include file="header.tpl"}
<body class="page-body page-fade gray">
<div class="page-container">
    {include file="side-bar.tpl"}

    <div class="main-content">
        {include file="top-bar.tpl"}

        <div class="row">
            <div class="col-md-12">

                <div class="well well-sm">
                    <h4><i class="entypo-layout"></i> List of Targets
                        <p class="pull-right">
                            <a href="{$BASE_URL}targets/view-thumbnails/"><i class="entypo-folder"></i></a>
                        </p>
                    </h4>
                </div>

                <table class="table table-bordered table-striped datatable" id="table-2">
                    <thead>
                    <tr>
                        <th class="text-center"><i class="entypo-arrow-combo"></i></th>
                        <th>Target Title</th>
                        <th class="text-center">Size</th>
                        <th class="text-center">Dimension</th>
                        <th class="text-center">File Type</th>
                        <th class="text-center">Sync. Status</th>
                        <th></th>
                        <th class="text-center">
                            <!--<div class="checkbox checkbox-replace">
                                <input type="checkbox" id="chk-1">
                            </div>-->
                        </th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        {foreach from=$target_list item=target key=ek}
                            <tr {if $target.sync_flag}class="text-success"{/if}>
                                <td class="text-center">{$ek+1}</td>
                                <td>
                                    <a href="#">
                                        <i class="entypo-vcard"></i>
                                    </a>
                                    {$target.title}
                                </td>
                                <td class="text-center">{$target.image_size} KB</td>
                                <td class="text-center">{$target.image_width}px by {$target.image_height}px</td>
                                <td class="text-center">{$target.image_type}</td>
                                <td class="text-center">
                                    {if $target.sync_flag}
                                        <i class="text-success entypo-progress-3 popover-primary" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="{$target.title} successfully synced at {$target.sync_date|default:''}" data-original-title="Successful Sync."></i>
                                    {else}
                                        <i class="text-danger entypo-progress-1" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Failed to sync. {$target.title}" data-original-title="Failed Sync."></i>
                                    {/if}
                                </td>
                                <td class="text-center">
                                    {if !$target.sync_flag}
                                    <a href="{$BASE_URL}targets/edit/{$target.target_id}/modify/" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>Edit</a>
                                    {else}
                                        <small>Not Editable</small>
                                    {/if}
                                </td>
                                <td class="text-center">
                                    <div class="checkbox checkbox-replace">
                                        <input type="checkbox" id="chk-1" name="target_check[]" value="{$target.target_id}">
                                    </div>
                                </td>
                                <td class="text-center">{$target.target_id}</td>
                            </tr>
                        {/foreach}
                    </tbody>
                </table>

                <div class="text-right">
                    {if $paginate_data.links}
                        {$paginate_data.links}
                        <p>{*$paginate_data.page_message*}</p>
                    {/if}
                </div>

                {literal}
                <script type="text/javascript">
                    jQuery(window).load(function() {
                        var $ = jQuery;
                        $("#table-2").dataTable({
                            "sPaginationType": "bootstrap",
                            "sDom": "t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
                            "bStateSave": false,
                            "iDisplayLength": 20,
                            "aoColumns": [{
                                "bSortable": false
                            },
                                null,
                                null,
                                null,
                                null
                            ]
                        });
                        $(".dataTables_wrapper select").select2({
                            minimumResultsForSearch: -1
                        });
                        // Highlighted rows
                        $("#table-2 tbody input[type=checkbox]").each(function(i, el) {
                            var $this = $(el),
                                    $p = $this.closest('tr');
                            $(el).on('change', function() {
                                var is_checked = $this.is(':checked');
                                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
                            });
                        });
                        // Replace Checboxes
                        $(".pagination a").click(function(ev) {
                            replaceCheckboxes();
                        });
                    });
                    // Sample Function to add new row
                    var giCount = 1;

                    function fnClickAddRow() {
                        $('#table-2').dataTable().fnAddData(['<div class="checkbox checkbox-replace"><input type="checkbox" /></div>', giCount + ".2", giCount + ".3", giCount + ".4", giCount + ".5"]);
                        replaceCheckboxes(); // because there is checkbox, replace it
                        giCount++;
                    }
                </script>
                {/literal}
            </div>
        </div>
        {include file="bottom-bar.tpl"}
    </div>
{include file="footer.tpl"}