{include file="header.tpl"}
<body class="page-body page-fade gray">
<div class="page-container">
    {include file="side-bar.tpl"}

    <div class="main-content">
        {include file="top-bar.tpl"}

        <div class="row">
            <div class="col-md-12">

                <div class="well well-sm">
                    <h4><i class="entypo-layout"></i> Create new Account</h4>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-body">

                        {if $error|default:''}
                            <div class="alert alert-danger">
                                <h3>Opps! Error Creating Account</h3>
                                {foreach from=$error item=err}
                                    <p>{$err}</p>
                                {/foreach}
                            </div>
                        {/if}

                        {if $okay|default:''}
                            <div class="alert alert-success">
                                <h3>Hey! Success Creating Account</h3>
                                <p>{$okay}</p>
                            </div>
                        {/if}

                        <form role="form" id="form1" action="{$BASE_URL}account/create/" method="post" class="validate">
                            <div class="form-group">
                                <label class="control-label">Account Name</label>
                                <input type="text" class="form-control" name="name" data-validate="required" data-message-required="This is field is required" placeholder="Enter Account Name" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Company</label>
                                <input type="text" class="form-control" name="company" data-validate="required" data-message-required="This is field is required" placeholder="Enter Company Name" />
                            </div>

                            <div class="form-group">
                                <label class="control-label">Contact Email</label>
                                <input type="email" class="form-control" name="contact_email" data-validate="required" data-message-required="This is field is required" placeholder="Enter Valid Email Address" />
                            </div>

                            <div class="form-group">
                                <label class="control-label">Description</label>
                                <textarea class="form-control autogrow" name="description" data-validate="required" data-message-required="This is field is required"></textarea>
                            </div>

                            <div class="form-group text-right">
                                <button type="reset" class="btn">Reset</button>
                                <button type="submit" class="btn btn-success"><i class="entypo-check"></i> Create Account</button>
                            </div>
                        </form>
                    </div>
                </div>


            </div>
        </div>
        {include file="bottom-bar.tpl"}
    </div>
{include file="footer.tpl"}