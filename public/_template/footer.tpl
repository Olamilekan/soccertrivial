
<!-- Imported styles on this page -->
<link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/jcrop/jquery.Jcrop.min.css">
<link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/dropzone/dropzone.css">
<link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/rickshaw/rickshaw.min.css">

<link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/select2/select2.css">


<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/gsap/main-gsap.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/bootstrap.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/joinable.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/resizeable.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/neon-api.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/cookies.min.js"></script>

<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/jquery.dataTables.min.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/datatables/TableTools.min.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/dataTables.bootstrap.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/datatables/lodash.min.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/datatables/responsive/js/datatables.responsive.js"></script>


<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/jcrop/jquery.Jcrop.min.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/dropzone/dropzone.js"></script>

<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/jquery.sparkline.min.js">
</script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/rickshaw/vendor/d3.v3.js" id="script-resource-11">
</script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/rickshaw/rickshaw.min.js" id="script-resource-12">
</script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/raphael-min.js" id="script-resource-13">
</script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/morris.min.js" id="script-resource-14"></script><script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/toastr.js" id="script-resource-15">
</script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/fullcalendar/fullcalendar.min.js" id="script-resource-16">
</script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/neon-chat.js" id="script-resource-17">
</script>
<!-- JavaScripts initializations and stuff -->
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/neon-custom.js" id="script-resource-18">
</script>
<!-- Demo Settings -->
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/neon-demo.js" id="script-resource-19">
</script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/js/neon-skins.js" id="script-resource-20">
</script>
</body>
</html>