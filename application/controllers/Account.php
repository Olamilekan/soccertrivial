<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends Auth_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('account_model','account');
    }


    public function index() {
        //$this->smarty->view('new-account.tpl');
    }

    public function create(){
        $data=array();

        if(!empty($_POST)){

            $name = addslashes($this->input->post('name'));
            $company = addslashes($this->input->post('company'));
            $email = addslashes($this->input->post('contact_email'));
            $description = $this->input->post('description');

            $error = array();

            if(empty($name)) $error[] = 'Empty Account Name';
            if(empty($company)) $error[] = 'Unknown Company Name';
            if(empty($email)) $error[] = 'Provide Valid Email Address';

            if(empty($error)){
                $this->load->model('account_model','account');
                $data = array('name'=>$name,'description'=>$description,'company'=>$company,'contact_email'=>$email);
                $status = $this->account->add($data);

                if($status)
                    $data['okay'] = 'Account Named "'.$name.'" successfully created.';
                else
                    $data['error'] = array('Seems Account Information already Exist');

            }else{
                $data['error'] = $error;
            }
        }
        $this->smarty->view('account-create.tpl', $data);
    }


    public function view(){
        //load all targets per pagination....
        $this->load->model('account_model','account');

        $condition = array();
        $total = $this->account->get_total($condition);
        $paginate = $this->set_paginator('account/view/',$total,3);
        $data['result_count'] = $total;
        $data['paginate_data'] = $paginate;
        $data['account_list'] = $this->account->get_all($this->config->item('paginator'),$paginate['page'],$condition);

        $this->smarty->view('account-view.tpl', $data );

    }
}
