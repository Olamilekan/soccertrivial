<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Targets extends Auth_Controller {

    public function index(){
        redirect('targets/view/');
    }


    public function create(){
        $data = array();
        $this->smarty->view('target-create.tpl',$data);
    }



    public function edit(){
        $data = array();
        echo 'Coming Soon....';
    }


    public function view(){
        $data = array();
        //load all targets per pagination....
        $this->load->model('target_model','target');

        $condition = array();
        $total = $this->target->get_total($condition);
        $paginate = $this->set_paginator('targets/view/',$total,3);
        $data['result_count'] = $total;
        $data['paginate_data'] = $paginate;
        $data['target_list'] = $this->target->get_all($this->config->item('paginator'),$paginate['page'],$condition);

        $this->smarty->view('target-view.tpl', $data );
    }


    public function view_thumbnails(){
        $data = array();
        //load all targets per pagination....
        $this->load->model('target_model','target');

        $condition = array();
        $total = $this->target->get_total($condition);
        $paginate = $this->set_paginator('targets/view/',$total,3);
        $data['result_count'] = $total;
        $data['paginate_data'] = $paginate;
        $data['target_list'] = $this->target->get_all($this->config->item('paginator'),$paginate['page'],$condition);

        $this->smarty->view('target-view-thumbnails.tpl', $data );
    }


    public function upload(){

        $config['upload_path']          = $this->config->item('target_folder');
        $config['allowed_types']        = $this->config->item('target_extension');
        $config['max_size']             = $this->config->item('target_size_limit');
        $config['encrypt_name']         = TRUE;

        $response = array();
        $status_code = 301;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file')) {
            $response = array('status' => false,'error' => $this->upload->display_errors());
        }else{

            $this->load->model('target_model','target');

            $upload = $this->upload->data();

            $data = array('title'=>$upload['orig_name'],'image_path'=>$upload['file_name'],'image_size'=>$upload['file_size'],'image_dimension'=>$upload['image_size_str'],'image_width'=>$upload['image_width'],'image_height'=>$upload['image_height'],'image_type'=>$upload['file_ext']);

            $status = $this->target->add($data);

            if($status){
                $response = array('status'=>true,'upload_data' => 'Successfully Uploaded');
                $status_code = 200;
            }else
                $response = array('status'=>false,'error' => 'Error adding Target to Database');
        }

        $this->output
            ->set_status_header($status_code) //$status_code
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }

}
