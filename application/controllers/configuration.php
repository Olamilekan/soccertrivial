<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuration extends Auth_Controller {

	var $data = array();

	function __construct(){
		parent::__construct();
		
		$this->load->model('miscellaneous_model','misscell');
	}

	function index(){
		if(!empty($_POST)){
			$update_counter = 0;
			foreach($_POST as $key=>$value){
			   if($value != '' && $key != ''){
					$update_status = $this->misscell->update_config(array('value'=>$value),array('label'=>strtoupper($key)));
					if($update_status)
						$update_counter++;
			   }
			}
			if($update_counter)
				$this->data['okay'] = $update_counter.' configuration row(s) successfully updated.';
			else
				$this->data['error'] = array('No configuration parameter updated.');
		}
		
		$config_data =  $this->misscell->load_config(array());

		if(!empty($config_data)){
			$this->data['config_parameter'] = $config_data;
		}else
			$this->data['error'] = 'No Configuration Parameter Found.';
		$this->smarty->view('configuration.tpl', $this->data );
	}
}

/* End of file Welcome.php */
/* Location: ./system/application/controllers/Welcome.php */