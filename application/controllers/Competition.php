<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Competition extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('participant_model','pModel');
        //$this->load->library('TwitterDriver',array(),'twitterDriver');
    }


    public function index() {
        redirect('competition/welcome/');
    }

    public function welcome(){
        $data = $participant_profile = array();
        $participant_id = $this->session->userdata('participant_identity');

        if(isset($participant_id) && !empty($participant_id)){
            $participant_profile = $this->pModel->get(array('participant_id'=>$participant_id));
            $data['participant_profile'] = $participant_profile;
            $data['campaign_url'] = $this->config->item('campaign_endpoint').'?user-data='.urlencode(json_encode($participant_profile));
        }

        /*if(empty($participant_profile)) {
            $tw_connect = $this->twitterDriver->set_request_token(base_url() . 'competition/tw-session-creation/?redirect-url=' . urlencode(base_url() . 'competition/update-profile/'));
            $data['tw_login_url'] = $tw_connect->url('oauth/authorize', array('oauth_token' => $this->session->userdata('tw_oauth_token')));
        }*/
        $this->smarty->view('competition/welcome.tpl', $data );
    }





    public function tw_session_creation(){
        $redirect_url = $_REQUEST['redirect-url'];

        $access_token = $this->twitterDriver->get_user_token($_REQUEST);
        $profile = $this->twitterDriver->get_user_data($access_token);

        /*
        $profile_array = array('id_str'=>'123087898','first_name'=>'Oluwasegun Matthew','screen_name'=>'Segzpair','location'=>'Ile-Ife',
                                'url'=>'http://google.com','description'=>'UX Analytic Guy',
                                'profile_image_url'=>'https://g.twimg.com/Twitter_logo_blue.png','');
        $profile = (object) $profile_array;
        */

        if(isset($profile->errors) && !empty($profile->errors)){
            redirect('competition/welcome/');
        }else {

            if (!empty($profile)) {

                $tw_profile_data = array('id_str' => $profile->id_str, 'name' => $profile->name,
                    'url' => $profile->url, 'location' => $profile->location, 'screen_name' => $profile->screen_name,
                    'description' => $profile->description,'profile_image_url' => $profile->profile_image_url,
                    'followers_count'=>$profile->followers_count,'friends_count'=>$profile->friends_count,
                    'tw_oauth_token' => $access_token['oauth_token'],
                    'tw_oauth_token_secret' => $access_token['oauth_token_secret']);

                $participant =  $this->participantModel->add($tw_profile_data);

                if(!empty($participant)) {
                    $this->session->set_userdata('participant_identity', $participant['participant_id']);
                    redirect('competition/finish/');
                }
            }
        }

    }


    public function finish(){
        $data=array();
        $participant_id = $this->session->userdata('participant_identity');

        if(!empty($_POST)){
            $error = array();

            $email = addslashes($this->input->post('email'));
            $phone = addslashes($this->input->post('phone'));
            $gender = addslashes($this->input->post('gender'));
            $age = $this->input->post('age');
            $state = $this->input->post('state');

            if(empty($email)) $error[] = 'Invalid Email Address';
            if(empty($phone)) $error[] = 'Provide Mobile Number';
            if(empty($gender)) $error[] = 'Unknown Gender';
            if(empty($age)) $error[] = 'Provide your real Age';
            if(empty($state)) $error[] = 'Unkown State of Origin';

            if(empty($error)){

                $data = array('email_address'=>$email,'gender'=>$gender,'age'=>$age,
                                'state_of_origin'=>$state,'phone_number'=>$phone,'complete_status'=>1);

                $status = $this->participantModel->update($data,array('participant_id'=>$participant_id));

                if($status) {
                    //Redirect here okay...
                    $user_info = $this->participantModel->get(array('participant_id'=>$participant_id));
                    $campaign_url = $this->config->item('campaign_endpoint').'?user-data='.urlencode(json_encode($user_info));
                    redirect($campaign_url);
                }
                else
                    $data['error'] = array('Error occured updating profile');

            }else{
                $data['error'] = $error;
            }
        }

        if(isset($participant_id) && !empty($participant_id)){
            $participant_profile = $this->participantModel->get(array('participant_id'=>$participant_id));
            $data['participant_profile'] = $participant_profile;

            if($participant_profile['complete_status']){
                $campaign_url = $this->config->item('campaign_endpoint').'?user-data='.urlencode(json_encode($participant_profile));
                redirect($campaign_url);
            }
        }else{
            redirect('competition/welcome/');
        }
        $this->smarty->view('competition/finish.tpl', $data);
    }


    public function end_point(){
        $user_data = json_decode(urldecode($this->input->get('user-data')));
        echo '<pre>';
            print_r($user_data);
        echo '</pre>';
    }


}
