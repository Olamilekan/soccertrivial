<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Participant_Model extends CI_Model {
	
	private $participant =  null;

	function __construct(){
		parent::__construct();
		$this->participant = Options::$_participants;
	}


	public function get_all($start,$limit,$condition=array(),$order="created_on desc"){
		$query = $this->db->order_by($order);
		if(!empty($condition)){
			$query = $query->get_where($this->participant, $condition,$start,$limit);
		}else{
			$query = $query->get($this->participant,$start,$limit);
		}
		$data = $query->result_array();
		return $data;
	}

	public function get_total($condition=array()){
		return $this->db->where($condition)->from($this->participant)->count_all_results();
	}


	public function get($condition){
		$query = $this->db->get_where($this->participant, $condition);
		$data = $query->row_array();
		if(!empty($data)){
			return $data;
		}else{
			return false;
		}
	}

	public function check($condition){
		$query = $this->db->get_where($this->participant, $condition);
		$data = $query->row_array();
		if(!empty($data)){
			return $data;
		}else{
			return false;
		}
	}


	public function add($data){
		$check = $this->check(array('id_str'=>$data['id_str']));
		if($check){
			return $check;
		}else {
			$this->db->insert($this->participant, $data);
			return $this->check(array('id_str'=>$data['id_str']));
		}
	}

	public function update($data,$condition){
		$this->db->update($this->participant, $data, $condition);
		if($this->db->affected_rows()){
			$update_data = $this->get($condition);
			return $update_data['participant_id'];
		}

	}

	public function delete($condition){
		$this->db->delete($this->participant, $condition);
		return $this->db->affected_rows();
	}


}
