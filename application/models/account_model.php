<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account_Model extends CI_Model {

	private $account = false;

	function __construct(){
		parent::__construct();
		$this->account = Options::$_account;
	}

	public function get_all($start,$lim='',$condition=array(),$order="account_id desc"){
		$query = $this->db->order_by($order);
		$limit = '';
		if(!empty($lim))
			$limit = $lim;

		if(!empty($condition)){
			$query = $query->get_where($this->account, $condition,$start,$limit);
		}else{
			$query = $query->get($this->account,$start,$limit);
		}
		$data = $query->result_array();
		return $data;
	}

	public function get_total($condition=array()){
		return $this->db->where($condition)->from($this->account)->count_all_results();
	}


	public function get($condition){
		$query = $this->db->get_where($this->account, $condition);
		$data = $query->row_array();
		if(!empty($data)){
			return $data;
		}else{
			return false;
		}
	}

	public function check($condition){
		$query = $this->db->get_where($this->account, $condition);
		$data = $query->row_array();
		if(!empty($data)){
			return $data;
		}else{
			return false;
		}
	}


	public function add($data){
		$status = $this->check(array('company'=>$data['company']));
		if(empty($status)) {
			$this->db->insert($this->account, $data);
			return $this->db->insert_id();
		}else
			return false;
	}

	public function update($data,$condition){
		$this->db->update($this->account, $data, $condition);
		if($this->db->affected_rows()){
			$update_data = $this->get($condition);
			return $update_data['target_id'];
		}

	}

	public function delete($condition){
		$this->db->delete($this->account, $condition);
		return $this->db->affected_rows();
	}

}