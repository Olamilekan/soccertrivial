<?php
/**
 * Created by PhpStorm.
 * User: lekanterragon
 * Date: 5/12/16
 * Time: 10:15 AM
 */

 if (! defined('BASEPATH')) exit('No direct script access allowed');

class User_data_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        //$tableName = 'users';
        $primaryKey = 'id';
        $this->load->library("Connector");
        $this->connection = $this->connector->Mongo();
    }

    function add_user($data){
        $this->connection->selectCollection("user_data","user")->insert($data);
        echo 'New User Added';
    }


}