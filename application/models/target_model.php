<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @author		Oluwasegun Matthew (07060514642)
 * @email		oadetimehin@terragonltd.com
*/

class Target_model extends CI_Model {

	private $target = false;

	function __construct(){
		parent::__construct();
		$this->target = Options::$_targets;
	}


	public function get_all($start,$limit='',$condition=array(),$order="target_id desc"){

		$query = $this->db->order_by($order);
		if(!empty($condition)){
			$query = $query->get_where($this->target, $condition,$start,$limit);
		}else{
			$query = $query->get($this->target,$start,$limit);
		}
		$data = $query->result_array();
		return $data;
	}

	public function get_total($condition=array()){
		return $this->db->where($condition)->from($this->target)->count_all_results();
	}


	public function get($condition){
		$query = $this->db->get_where($this->target, $condition);
		$data = $query->row_array();
		if(!empty($data)){
			return $data;
		}else{
			return false;
		}
	}

	public function check($condition){
		$query = $this->db->get_where($this->target, $condition);
		$data = $query->row_array();
		if(!empty($data)){
			return $data;
		}else{
			return false;
		}
	}


	public function add($data){
		$this->db->insert($this->target, $data);
		return $this->db->insert_id();
	}

	public function update($data,$condition){
		$this->db->update($this->target, $data, $condition);
		if($this->db->affected_rows()){
			$update_data = $this->get($condition);
			return $update_data['target_id'];
		}

	}

	public function delete($condition){
		$this->db->delete($this->target, $condition);
		return $this->db->affected_rows();
	}

}
