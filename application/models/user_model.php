<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Model extends CI_Model {
	
	private $admin =  null;

	function __construct(){
		parent::__construct();
		$this->admin = Options::$_admin;
	}


	public function get_all($start,$limit,$condition=array(),$order="date_created desc"){
		$query = $this->db->order_by($order);
		if(!empty($condition)){
			$query = $query->get_where($this->admin, $condition,$start,$limit);
		}else{
			$query = $query->get($this->admin,$start,$limit);
		}
		$data = $query->result_array();
		return $data;
	}

	public function get_total($condition=array()){
		return $this->db->where($condition)->from($this->admin)->count_all_results();
	}


	public function get($condition){
		$query = $this->db->get_where($this->admin, $condition);
		$data = $query->row_array();
		if(!empty($data)){
			return $data;
		}else{
			return false;
		}
	}

	public function check($condition){
		$query = $this->db->get_where($this->admin, $condition);
		$data = $query->row_array();
		if(!empty($data)){
			return $data;
		}else{
			return false;
		}
	}


	public function add($data){
		$this->db->insert($this->admin, $data);
		return $this->db->insert_id();
	}

	public function update($data,$condition){
		$this->db->update($this->admin, $data, $condition);
		if($this->db->affected_rows()){
			$update_data = $this->get($condition);
			return $update_data['target_id'];
		}

	}

	public function delete($condition){
		$this->db->delete($this->admin, $condition);
		return $this->db->affected_rows();
	}

	/*public function authenticate($username,$password){
		$query=$this->db->query("SELECT * FROM vuforia_admin WHERE username='$username' AND passkey='$password'");
		if($query->num_rows() > 0){
        	return $query->row_array();
       	}
        else {
           return false;
        }
    }*/

}
