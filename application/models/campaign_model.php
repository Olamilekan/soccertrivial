<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Campaign_Model extends CI_Model {

	private $campaign = false;
	private $account = false;

	function __construct(){
		parent::__construct();
		$this->campaign = Options::$_campaign;
		$this->account = Options::$_account;
	}

	public function get_all($start,$limit,$condition=array(),$order="campaign_id desc"){
		$query = $this->db->order_by($order)
			->join($this->account, $this->campaign.'.account_id = '.$this->account.'.account_id', 'left');
		if(!empty($condition)){
			$query = $query->get_where($this->campaign, $condition,$start,$limit);
		}else{
			$query = $query->get($this->campaign,$start,$limit);
		}
		$data = $query->result_array();
		return $data;
	}

	public function get_total($condition=array()){
		return $this->db->where($condition)->from($this->campaign)->count_all_results();
	}


	public function get($condition){
		$query = $this->db->get_where($this->campaign, $condition);
		$data = $query->row_array();
		if(!empty($data)){
			return $data;
		}else{
			return false;
		}
	}

	public function check($condition){
		$query = $this->db->get_where($this->campaign, $condition);
		$data = $query->row_array();
		if(!empty($data)){
			return $data;
		}else{
			return false;
		}
	}


	public function add($data){
		$status = $this->check(array('title'=>$data['title']));
		if(empty($status)) {
			$this->db->insert($this->campaign, $data);
			return $this->db->insert_id();
		}else
			return false;
	}

	public function update($data,$condition){
		$this->db->update($this->campaign, $data, $condition);
		if($this->db->affected_rows()){
			$update_data = $this->get($condition);
			return $update_data['target_id'];
		}
	}

	public function delete($condition){
		$this->db->delete($this->campaign, $condition);
		return $this->db->affected_rows();
	}


/*
    function __construct(){
        parent::__cstruct();
    }

    /*public function create_campaign($account, $title, $description, $duration){
        $query=$this->db->query("SELECT * FROM vuforia_campaigns WHERE title ='$title'");

        if($query->num_rows()>0){
            return Options::response(false,null,"Campaign Name already exist");
        }

        else{
            $account_query = $this->db->query("INSERT INTO vuforia_campaigns (account_id,title,description,duration ) VALUES ('$account','$title','$description','duration')");
            $data=$query->num_rows();
            return Options::response(true,$data,"Campaign Creation successful!");
        }

    }


    public function view_account(){
        $query = $this->db->query("SELECT * FROM vuforia_campaigns");
        //$data = $query->num_rows();
        $data = $query->result_array();
        return $data;

    }

    public function account_count(){
        $query = $this->db->query("SELECT * FROM vuforia_campaigns");
        $data=$query->num_rows();
        //return Options::response(true,$query,null);
        return $data;
    }
    */

}