<?php 
namespace libraries;

defined( 'BASEPATH' ) or die( 'Restricted access');

class Response {
	var $responseArray = array(
		'status' 		=> false,
		'error_code'	=> 0,
		'response' 		=> ''		
	);
	
	function response_ok($response){
		$this->responseArray['status'] = true;
		$this->responseArray['response'] = $response;
		$this->response($this->responseArray,200);
	}
	function response_bad($response,$error_code){
		$this->responseArray['status'] = false;
		$this->responseArray['response'] = $response;
		$this->responseArray['error_code'] = $error_code;
		$this->response($this->responseArray,400);

	}

	function response($data,$error_code)
	{		
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Content-Type');
		header('Content-Type: application/json');	
		echo json_encode($data);
		exit;				
	}
	
	
}


