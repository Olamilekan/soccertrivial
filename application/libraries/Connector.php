<?php 
//namespace libraries;
//use libraries\Config;


defined( 'BASEPATH' ) or die( 'Restricted access');

//use libraries\Config;

class Connector {
    //public $redis;
//    public $elastic;
    //public $rabbitMQ;
    //private static $instances;
    public $MongoDB;


        public function __construct() {

        }

/*
        private function __construct() {
            $redis_params = Config::read('redis');
            $elastic_params = Config::read('elastic');

            $this->elastic = new Elastic($elastic_params);
            $this->redis = new Predis($redis_params);

            $this->redis->connect();
            $this->elastic->connect();
        }

        public static function getInstance() {
            if (!isset(self::$instance))
            {
                $object = __CLASS__;
                self::$instance = new $object;
            }
            return self::$instance;
        }*/


    /*public static function Redis(){
        static $instances;
        if (!isset ($instances))    $instances = array ();
        if (empty($instances['redis'])) {
            $param = Config::read('redis');
            $redis = null;

            try {
                $redis = new \Predis\Client(array(
                    "scheme"    => $param['redis_scheme'],
                    "host"      => $param['redis_host'],
                    "port"      => $param['redis_port'],
                    "database"  => $param['redis_database'],
					"password"	=> $param['redis_password']
                ));
            }
            catch (Exception $e) {
                show_error("Unable to connect to Redis: {$e->getMessage()}", 500);
            }
            $instances['redis'] = $redis;            
        }
        return $instances['redis'];
    }*/





//    public static function Elastic(){
//        static $instances;
//
//        if (!isset ($instances))    $instances = array ();
//        if (empty($instances['elastic'])) {
//            $param = Config::read('elastic');
//            $elastic = null;
//
//            try {
//                $elastic = new \Elasticsearch\Client(array(
//                    "hosts"                 => $param['elastic_host'],
//                    "sniffOnStart"          => false,
//                    "connectionParams"      => array(),
//                    "logging"               => false,
//                    "logObject"             => null,
//                    "logPath"               => 'elasticsearch.log',
//                    // "logLevel"              => $this->logLevel,
//                    "traceObject"           => null,
//                    "tracePath"             => 'elasticsearch.log',
//                    //"traceLevel"            => $this->traceLevel,
//                    "guzzleOptions"         => array(),
//                    "connectionPoolParams"  => array('randomizeHosts' => true),
//                    "retries"               => null
//                ));
//            }
//            catch (Exception $e) {
//                show_error("Unable to connect to Elastic search: {$e->getMessage()}", 500);
//            }
//
//            $instances['elastic'] = $elastic;
//        }
//        return $instances['elastic'];
//    }



//    public static function AeroSpike(){
//        static $instances;
//        if (!isset ($instances))    $instances = array ();
//        if (empty($instances['aerospike'])) {
//            $param = Config::read('aerospike');
//            $aerospike = null;
//
//            $config = [
//            "hosts" => [
//                [ "addr" => "127.0.0.1", "port" => 3000 ]]];
//
//            // The new client will connect and learn the cluster layout
//            $aerospike = new Aerospike($config,false);
//
//            try {
//                $config = array("hosts"=>array("addr"=>$param['aero_host'],"port"=>$param['aero_port']));
//                $aerospike = new AeroSpike($config);
//            }
//            catch (Exception $e) {
//                show_error("Unable to connect to AeroSpike Server: {$e->getMessage()}", 500);
//            }
//
//            $instances['aerospike'] = $aerospike;
//        }
//        return $instances['aerospike'];
//    }




    public function Mongo(){
        static $instances;
        if (!isset($instances))
            $instances = array();
        if (empty($instances['MongoDB'])){
            //$config = $this->load->config("mongo_db");
            //$param = Config::read('MongoDB');
            $MongoDB = null;

            try{
                $MongoDB = new \MongoClient("mongodb://localhost:27017");
                $MongoDB->selectDB("user_data");
            }
            catch (\Exception $e)
            {
                show_error("Unable to connect to MongoDB: {$e->getMessage()}", 500);
            }
            $instances['MongoDB'] = $MongoDB;
        }
        return $instances['MongoDB'];



    }

    

    /*public static function Rabbit(){
        static $instances;

        if (!isset ($instances))    $instances = array ();
        if (empty($instances['rabbitMQ'])) {
            $param = Config::read('rabbitMQ');
            $rabbitMQ = null;
            
            try {
                $rabbitMQ = new \PhpAmqpLib\Connection\AMQPConnection(
                                        $param['rabbit_host'],
                                        $param['rabbit_port'],
                                        $param['rabbit_username'],
                                        $param['rabbit_passkey']);
                $rabbitMQ = $rabbitMQ->channel();                
            }
            catch (Exception $e) {
                show_error("Unable to connect to RabbitMQ: {$e->getMessage()}", 500);
            }
            $instances['rabbitMQ'] = $rabbitMQ;
        }
        return $instances['rabbitMQ'];

    }*/
    
    // others global functions
}