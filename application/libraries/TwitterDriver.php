<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: oluwasegunmatthew
 * Date: 7/11/15
 * Time: 12:43 PM
 */

use Abraham\TwitterOAuth\TwitterOAuth;
use Abraham\TwitterOAuth\TwitterOAuthException;

class TwitterDriver {

    protected $_ci;

    private $tw_consumer_key;
    private $tw_secret_key;

    private $tw_session = array();
    private $tw_request_token = array();

    private $tw_connect = null;
    private $access_token = null;

    function __construct(){
        $this->_ci = &get_instance();

        $this->tw_consumer_key = $this->_ci->config->item('consumer_key');
        $this->tw_secret_key = $this->_ci->config->item('consumer_secret');
        $this->tw_oauth_callback = $this->_ci->config->item('oauth_callback');
    }

    public function init_connect(){
        try {
            $this->tw_connect = new TwitterOAuth($this->tw_consumer_key, $this->tw_secret_key);
            return $this->tw_connect;
        }catch (TwitterOAuthException $ex){
            //print_r($ex);
        }
    }



    public function set_request_token($callback_url = ''){

        $client = $this->init_connect();

        if($callback_url == '') $callback_url = $this->tw_oauth_callback;
        if($this->tw_connect != null) {
            try {
                $this->tw_request_token = $this->tw_connect->oauth('oauth/request_token', array('oauth_callback' => $callback_url));
                if (!empty($this->tw_request_token)) {
                    $this->_ci->session->set_userdata('tw_oauth_token', $this->tw_request_token['oauth_token']);
                    $this->_ci->session->set_userdata('tw_oauth_token_secret', $this->tw_request_token['oauth_token_secret']);
                    //$_SESSION['tw_oauth_token'] = $this->tw_request_token['oauth_token'];
                    //$_SESSION['tw_oauth_token_secret'] = $this->tw_request_token['oauth_token_secret'];
                }
            } catch (TwitterOAuthException $ex) {
                //print_r($ex);
            }
        }

        return $client;
    }

    public function get_user_token($request_data){
        $request_token = array();
        $request_token['oauth_token'] = $this->_ci->session->userdata('tw_oauth_token'); //@$_SESSION['tw_oauth_token'];
        $request_token['oauth_token_secret'] = $this->_ci->session->userdata('tw_oauth_token_secret'); //@$_SESSION['tw_oauth_token_secret'];

        if(isset($request_data['denied']) || !empty($request_data['denied']))
            return null;
        else {
            if (isset($request_data['oauth_token']) && $request_data['oauth_token'] !== $request_data['oauth_token']) {
                // Abort! Something is wrong.
                return null;
            } else {
                try {
                    $this->tw_connect = new TwitterOAuth($this->tw_consumer_key, $this->tw_secret_key, $request_token['oauth_token'], $request_token['oauth_token_secret']);
                    $this->access_token = $this->tw_connect->oauth("oauth/access_token", array("oauth_verifier" => @$request_data['oauth_verifier']));
                    $this->_ci->session->set_userdata('tw_access_token', $this->access_token);
                    //$_SESSION['tw_access_token'] = $this->access_token;
                    return $this->access_token;
                }catch (TwitterOAuthException $ex){
                    //print_r($ex);
                }
            }
        }
    }


    public function get_user_data($access_token){
        try {
            $connection = new TwitterOAuth($this->tw_consumer_key, $this->tw_secret_key, $access_token['oauth_token'], $access_token['oauth_token_secret']);
            $user = $connection->get('account/verify_credentials');
            return $user;
        }catch (TwitterOAuthException $ex){
            //print_r($ex);
        }
    }


    public function get_friend_list($access_token){
        try {
            $connection = new TwitterOAuth($this->tw_consumer_key, $this->tw_secret_key, $access_token['oauth_token'], $access_token['oauth_token_secret']);
            $friends = $connection->get('friends/list');
            return $friends;
        }catch (TwitterOAuthException $ex){
            //print_r($ex);
        }
    }

}