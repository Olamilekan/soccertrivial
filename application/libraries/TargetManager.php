<?php
/**
 * Description of Options
 *
 * @author Oluwasegun
 */

set_include_path(get_include_path() . PATH_SEPARATOR . APPPATH.'libraries/Vuforia-Dependencies/');

require_once 'Vuforia-Dependencies/HTTP/Request2.php';
require_once 'Vuforia-Dependencies/SignatureBuilder.php';


class TargetManager {

    //Server Keys
    private $access_key 	        = "SERVER_ACCESS_KEY";
    private $secret_key 	        = "SERVER_SECRET_KEY";

    private $url 			        = "https://vws.vuforia.com";
    private $meta_data 			    = "Vuforia test metadata";//""
    private $requestPath 	        = "/targets";
    private $request;
    private $json_request_object;

    private $target_name 	        = "";
    private $image_location 	    = "";


    function __construct($param){
        $this->access_key = $param['access_key'];
        $this->secret_key = $param['secret_key'];
    }

    public function setTarget($param){
        $this->target_name = $param['target_name'];
        $this->image_location = $param['target_file'];

        $this->json_request_object = json_encode( array('width'=>(int)($param['target_width']),
                                                        'name'=>$this->target_name,
                                                        'image'=>$this->getImageAsBase64(),
                                                        'application_metadata'=>base64_encode($this->meta_data),
                                                        'active_flag'=>1)
                                                );
    }



    public function postTarget(){
        $data = array('status'=>false,'response'=>'');

        $this->request = new HTTP_Request2();
        $this->request->setMethod( HTTP_Request2::METHOD_POST );
        $this->request->setBody( $this->json_request_object );

        $this->request->setConfig(array(
            'ssl_verify_peer' => false
        ));
        $this->request->setURL( $this->url . $this->requestPath );
        // Define the Date and Authentication headers
        $this->setHeaders();
        $response = array();

        try {
            $response = $this->request->send();
            if (200 == $response->getStatus() || 201 == $response->getStatus() ) {
                $data['status'] = true;
            }
            $data['response'] = $response->getBody();
        } catch (HTTP_Request2_Exception $e) {
            $data['response'] = 'Error: '. json_encode($e->getMessage());
        }
        return $data;
    }

    private function getImageAsBase64(){
        $file = file_get_contents( $this->image_location );
        if( $file ){
            $file = base64_encode( $file );
        }
        return $file;
    }

    private function setHeaders(){
        $sb = 	new SignatureBuilder();
        $date = new DateTime("now", new DateTimeZone("GMT"));
        $this->request->setHeader('Date', $date->format("D, d M Y H:i:s") . " GMT" );
        $this->request->setHeader("Content-Type", "application/json" );
        $this->request->setHeader("Authorization" , "VWS " . $this->access_key . ":" .
            $sb->tmsSignature( $this->request , $this->secret_key ));
    }







}