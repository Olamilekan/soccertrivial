<?php

//namespace libraries;

// Configuration Class
class Config {
    static $confArray = array(
        "MongDB" => array(
            'mongo_host'  => 'localhost',
            'mongo_port'            => 27017,
            'mongo_user'            => '',
            'mongo_pass'        => '',
            'mongo_db'              => 'user_data',
            'mongo_replicaset'      => FALSE,
            'mongo_slave_ok'     => TRUE,
            'mongo_write_timeout'  => 5000,
            'mongo_ensure_replicas' => 0,
            'mongo_update_all'     => TRUE,
            'mongo_remove_all'      => TRUE,
            'mongo_use_upsert'      => TRUE,
            'mongo_expand_dbrefs'   => TRUE,
        )
    );

    public static function read($name) {
    	
        return self::$confArray[$name];
    }
//    public static function write($valueArray) {    	
//    	self::$confArray=$valueArray;   	
//        
//    }
}